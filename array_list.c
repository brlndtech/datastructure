/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "array_list.h"

void init_array_list(Array_list l);
void insert_at(Array_list l, int position, float value);
float remove_at(Array_list l, int position);
float get_at(Array_list l);
void clear(Array_list l)